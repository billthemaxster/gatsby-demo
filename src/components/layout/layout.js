import React from "react";
import ListLink from "./linkList";
import { StaticQuery, Link, graphql } from "gatsby";
import { css } from "react-emotion";

import { rhythm } from "../../utils/typography";

export default ({ children }) => (
  <StaticQuery
    query={graphql`
      query{
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <div
        className={css`
          margin: 0 auto;
          max-width: 700px;
          padding: ${rhythm(2)};
          padding-top: ${rhythm(1.5)};
        `}
      >
        <header style={{ marginBottom: '1.5rem' }}>
          <Link to="/">
            <h3
              className={css`
                margin-bottom: ${rhythm(2)};
                display: inline-block;
                font-style: normal;
              `}
            >
              {data.site.siteMetadata.title}
            </h3>
          </Link>
          <ul style={{ listStyle: 'none', float: 'right' }}>
            <ListLink to="/">Home</ListLink>
            <ListLink to="/about/">About</ListLink>
            <ListLink to="/contact/">Contact</ListLink>
            <ListLink to="/my-files">My Files</ListLink>
          </ul>
        </header>
        {children}
        <div>
          To C, from K
        </div>
      </div>
    )}
  />
)
