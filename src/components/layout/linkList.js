import React from "react";
import { Link } from "gatsby";

import { css } from "react-emotion";

export default (props) => (
  <li
    className={css`
      display: inline-block;
      margin-right: 1rem;
    `}
  >
    <Link to={props.to}>{props.children}</Link>
  </li>
)
