import React from "react";
import Header from "../components/header";
import Layout from "../components/layout/layout";

export default () => (
  <Layout>
    <Header headerText="Contact" />
    <p>Send us a message.</p>
  </Layout>
)
