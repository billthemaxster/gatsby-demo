import React from "react";
import Header from "../components/header";
import Layout from "../components/layout/layout";
import { css } from 'react-emotion';
import { rhythm } from '../utils/typography';
import { Link, graphql } from "gatsby";

export default ({ data }) => (
  <Layout>
    <Header headerText="Amazing Pandas Eating Things" />
    <h4>{data.allMarkdownRemark.totalCount} Posts</h4>
    {data.allMarkdownRemark.edges.map(({ node }) => (
      <div key={node.id}>
        <Link to={node.fields.slug}
          className={css`
            text-decoration: none;
            color: inherit;
          `}>
          <h3 className={css`margin-bottom: ${rhythm(1 / 4)};`}>
            {node.frontmatter.title}{" "}
            <span className={css`color: #bbb;`}>
              - {node.frontmatter.date}
            </span>
          </h3>
          <p>{node.excerpt}</p>
        </Link>
      </div>
    ))}
  </Layout>
)

export const query = graphql`
  query {
    allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}) {
      edges {
        node {
          frontmatter {
            title
            date
          }
          fields {
            slug
          }
          excerpt
        }
      }
      totalCount
    }
  }
`
