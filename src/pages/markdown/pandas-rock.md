---
title: "Pandas Rock"
date: "2018-10-27"
---

Pandas rock! 

Here's the evidence to prove it:

* They're cute AF
* They're hella dopey
* They're as smart as a rock

But wait! There's more ...

* They're highly endangered
* They eat objectively the least nutritious food

